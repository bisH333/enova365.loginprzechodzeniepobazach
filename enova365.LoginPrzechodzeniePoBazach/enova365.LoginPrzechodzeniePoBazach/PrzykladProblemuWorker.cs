﻿using enova365.LoginPrzechodzeniePoBazach;
using Soneta.Business;
using Soneta.Business.App;
using Soneta.Core;

[assembly: Worker(typeof(PrzykladProblemuWorker), typeof(DokEwidencji))]

namespace enova365.LoginPrzechodzeniePoBazach
{
    public class PrzykladProblemuWorker : ContextBase
    {
        public PrzykladProblemuWorker(Context context) : base(context) { }

        [Action("PrzykładLogin", Target = ActionTarget.Menu, Mode = ActionMode.Progress | ActionMode.SingleSession)]
        public void Action()
        {

            var listaBaz = new string[] { "test_N1", "test_N1 Serwis" };
            
            foreach (var baza in listaBaz)
            {
                /*
                 * Wcześniej to nie było Obsolete i działało
                 * Konfiguracja która jest od lat wygląda następująco:
                 * Mamy włączone logowanie zintegrowane
                 * Na każdej bazie występują jednakowi operatorzy
                 * Maja wpisane te same hasła
                 * Nigdy w życiu nie używaliśmy dll'ki PNWB
                 * 
                 * 
                 * Teraz to zadziała jeżeli usunę hasła operatorów - tego nie mogę zrobić na produkcji
                 * 
                 * 
                 * Tego typu rozwiązania stosujemy np. przy zakładaniu kont w planie kont na wielu firmach jednocześnie
                */
                using (var session = BusApplication.Instance[baza].Login(Session.Login).CreateSession(false, false))

                {

                    using (var trans = session.Logout(true))
                    {
 
                    }

                }

            }
        }

    }
}
